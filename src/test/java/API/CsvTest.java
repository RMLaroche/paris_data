package API;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.junit.Assert;
import org.junit.Test;

import CSVUtils.CSV;
import CSVUtils.CSVFormat;

public class CsvTest {

	@Test
	public void CsvCountLineTest() {

		String path = System.getProperty("user.dir");
		String csvFile = path + "/DataFile.csv";
        BufferedReader br = null;
        String line = "";
        int lineCount = 0;

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
            	lineCount++;
            }
            
            Assert.assertEquals(4, lineCount);
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
	@Test
	public void CsvCheckDataTest() {

		String path = System.getProperty("user.dir");
		String csvFile = path + "/DataFile.csv";
        BufferedReader br = null;
        String line = "";
        int lineCount = 0;
        String value = "";

        try {


            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
            	lineCount++;
            	
            	if (lineCount == 3) {
                    String[] lineData = line.split(",");
                    value = lineData[0];
            	}
            		
            }
            
            Assert.assertEquals("1/2/09 4:53", value);
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
	@Test
	public void BytesToCsvTest() {

		String path = System.getProperty("user.dir");
        BufferedReader br = null;
        String line = "";
        int lineCount = 0;
        String value = "";

        try {
        	
        	CSV csv = new CSV();
        	
        	File file = new File(path + "/DataFile.csv");
        	byte[] fileBytes = csv.CsvToByte(file);
        	String pathFileOutput = csv.ByteToCsv(fileBytes, path + "/test.csv");
        	
	        br = new BufferedReader(new FileReader(pathFileOutput));
            while ((line = br.readLine()) != null) {
            	lineCount++;
            	
            	if (lineCount == 3) {
                    String[] lineData = line.split(",");
                    value = lineData[0];
            	}
            		
            }
            
            br.close();
            Assert.assertEquals("1/2/09 4:53", value);
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
	@Test
	public void CsvReadDataHeaderTest() {

		String path = System.getProperty("user.dir");
        BufferedReader br = null;
        String line = "";
        int lineCount = 0;
        String value = "";

        try {
        	
        	CSV csv = new CSV();
        	
        	File file = new File(path + "/DataFile.csv");
        	CSVFormat data = csv.CsvReadData(file);
        	
            Assert.assertEquals("Transaction_date", data.Header.get(0));
            Assert.assertEquals("Product", data.Header.get(1));
            
        }catch(Exception e) {
        	
        }
	}
	
	@Test
	public void CsvReadDataBodyTest() {

		String path = System.getProperty("user.dir");
        BufferedReader br = null;
        String line = "";
        int lineCount = 0;
        String value = "";

        try {
        	
        	CSV csv = new CSV();
        	
        	File file = new File(path + "/DataFile.csv");
        	CSVFormat data = csv.CsvReadData(file);

            Assert.assertEquals("1/2/09 4:53", data.Body.get(3).get(0));
            Assert.assertEquals("Product2", data.Body.get(3).get(1));
            
        }catch(Exception e) {
        	
        }
	}
}
