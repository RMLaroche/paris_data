package api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import client.Entity.sys_information;
import client.Service.SystemeInformationService;

@Path("/systemeInformation")
public class SystemeInformationResource {

	private SystemeInformationService informationService = new SystemeInformationService();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<sys_information> getInfos() {
		return informationService.getInformationList();
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public sys_information getInfo(@PathParam("id") Integer id) {
		return informationService.getInformation(id);
	}
}
