package CSVUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CSVFormat {

	public List<String> Header = new ArrayList<String>();
	
	public Map<Integer, ArrayList<String>> Body = new HashMap<Integer, ArrayList<String>>();
}
