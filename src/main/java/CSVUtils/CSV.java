package CSVUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class CSV {

	public String ByteToCsv(byte[] bytes, String fileNameOutputWithPath) {

		try {
			OutputStream os = new FileOutputStream(fileNameOutputWithPath); 
			os.write(bytes); 
			os.close();
			return fileNameOutputWithPath;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public byte[] CsvToByte(File file) {

		try {

        	byte[] bytesArray = new byte[(int) file.length()];

        	FileInputStream fis = new FileInputStream(file);
        	fis.read(bytesArray); //read file into bytes[]
        	fis.close();
        	return bytesArray;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public CSVFormat CsvReadData(File file) {
		
        BufferedReader br = null;
        String line = "";
        int lineCount = 0;
        CSVFormat data = new CSVFormat();

        try {

            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
            	lineCount++;
            	
                String[] lineData = line.split(",");
                
            	if (lineCount == 1)
            	{
            		for (int i = 0; i < lineData.length; i++) {
            			data.Header.add(lineData[i]);
            		}
            	}
            	else {
            		var bodyTemp = new ArrayList<String>();
            		for (int i = 0; i < lineData.length; i++) {
            			bodyTemp.add(lineData[i]);
            		}
            		data.Body.put(lineCount, bodyTemp);
            	}
            }
            
            br.close();
            return data;
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return null;
	}
}
