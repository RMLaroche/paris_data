package client.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class type_utilisateur {


	public type_utilisateur(Integer id_type, String libelle_type) {
		this.id_type = id_type;
		this.libelle_type = libelle_type;
	}
	
	public type_utilisateur(){

	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id_type;
	
	private String libelle_type;

	public Integer getId_type() {
		return id_type;
	}

	public void setId_type(Integer id_type) {
		this.id_type = id_type;
	}

	public String getLibelle_type() {
		return libelle_type;
	}

	public void setLibelle_type(String libelle_type) {
		this.libelle_type = libelle_type;
	}
}
