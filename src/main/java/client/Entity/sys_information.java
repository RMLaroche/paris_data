package client.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class sys_information {

	public sys_information(Integer id_information, String nom_information, String valeur_information, Integer thematique_id) {
		this.id_information = id_information;
		this.nom_information = nom_information;
		this.valeur_information = valeur_information;
		this.thematique_id = thematique_id;
	}
	
	public sys_information(){

	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id_information;
	
	private String nom_information;
	
	private String valeur_information;
	
	private Integer thematique_id;
	
	public Integer getId_information() {
		return id_information;
	}

	public void setId_information(Integer id_information) {
		this.id_information = id_information;
	}

	public String getNom_information() {
		return nom_information;
	}

	public void setNom_information(String nom_information) {
		this.nom_information = nom_information;
	}
	
	public String getValeur_information() {
		return valeur_information;
	}

	public void setValeur_information(String valeur_information) {
		this.valeur_information = valeur_information;
	}
	
	public Integer getThematique_id() {
		return thematique_id;
	}

	public void setThematique_id(Integer thematique_id) {
		this.thematique_id = thematique_id;
	}
}
