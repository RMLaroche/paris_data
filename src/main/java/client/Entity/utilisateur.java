package client.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class utilisateur {

	public utilisateur(Integer id_utilisateur , String login, String mot_de_passe, Integer type_id) {
		this.id_utilisateur = id_utilisateur;
		this.login = login;
		this.mot_de_passe = mot_de_passe;
		this.type_id = type_id;
	}
	
	public utilisateur(){

	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id_utilisateur;
	
	private String login;
	
	private String mot_de_passe;
	
	private Integer type_id;
	
	public Integer getId_utilisateur() {
		return id_utilisateur;
	}

	public void setId_utilisateur(Integer id_utilisateur) {
		this.id_utilisateur = id_utilisateur;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getMot_de_passe() {
		return mot_de_passe;
	}

	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	}
	
	public Integer getType_id() {
		return type_id;
	}

	public void setType_id(Integer type_id) {
		this.type_id = type_id;
	}
}
