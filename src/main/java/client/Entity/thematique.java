package client.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class thematique {

	public thematique(Integer id_thematique  , String nom_thematique) {
		this.id_thematique  = id_thematique ;
		this.nom_thematique = nom_thematique;
	}
	
	public thematique(){

	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id_thematique ;
	
	private String nom_thematique;
	

	
	public Integer getId_thematique() {
		return id_thematique;
	}

	public void setId_thematique(Integer id_thematique) {
		this.id_thematique = id_thematique;
	}

	public String getLogin() {
		return nom_thematique;
	}

	public void setNom_thematique(String nom_thematique) {
		this.nom_thematique = nom_thematique;
	}
}
