package client.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import client.Entity.sys_information;

@Stateless
public class SystemeInformationDAO {


    public sys_information getByName(String nom) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("paris_data");

        EntityManager em = emf.createEntityManager();
        return em.createQuery("select s from sys_information s where s.nom_information = :name", sys_information.class)
                .setParameter("name", nom)
                .getSingleResult();
    }

    public List<sys_information> getInformations() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("paris_data");

        EntityManager em = emf.createEntityManager();

        return em.createQuery("select p from sys_information p", sys_information.class).getResultList();
    }

    public sys_information getSystemInformation(Integer identifier) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("paris_data");

        EntityManager em = emf.createEntityManager();

        return em.find(sys_information.class, identifier);
    }
}
