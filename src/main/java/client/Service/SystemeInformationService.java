package client.Service;

import java.util.List;

import client.Entity.sys_information;
import client.dao.SystemeInformationDAO;

public class SystemeInformationService {

	public List<sys_information> getInformationList() {
		SystemeInformationDAO systemeInformationDAO = new SystemeInformationDAO();
		return systemeInformationDAO.getInformations();
	}

	public sys_information getInformation(Integer identifier) {
		SystemeInformationDAO systemeInformationDAO = new SystemeInformationDAO();
		return systemeInformationDAO.getSystemInformation(identifier);
	}
}
